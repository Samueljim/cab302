package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import asgn1SoccerCompetition.SportsTeamForm;
import asgn1SportsUtils.WLD;

/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerTeamForm class
 *
 * @author Alan Woodley
 *
 */
public class SportsTeamFormTests {
	
	SportsTeamForm teamForm;
	
	@Before
	public void initial(){
		teamForm = new SportsTeamForm();
	}
	
	
	@Test
	public void addResultToFormTest(){
		teamForm.addResultToForm(WLD.WIN);
		teamForm.addResultToForm(WLD.LOSS);
		
		String form = teamForm.toString();		
		assertEquals("LW---", form);
	}
	//
	@Test
	public void addResultToFormTest1(){
		teamForm.addResultToForm(WLD.WIN);
		
		String form = teamForm.toString();		
		assertEquals("W----", form);
	}
	//tests to see if toString is able to return with nothing inputed   
	@Test 
	public void toStringTest(){
		
		String form = teamForm.toString();		
		assertEquals("-----", form);
	}
	
	//test what happens if you add too many match results, this should only return a string that's 5 chars 
	//as addResultToForm removes the last game
	@Test
	public void addResultTestFull(){
		teamForm.addResultToForm(WLD.WIN);
		teamForm.addResultToForm(WLD.DRAW);
		teamForm.addResultToForm(WLD.WIN);
		teamForm.addResultToForm(WLD.LOSS);
		teamForm.addResultToForm(WLD.WIN);
		teamForm.addResultToForm(WLD.LOSS);
		teamForm.addResultToForm(WLD.WIN);
		teamForm.addResultToForm(WLD.LOSS);
		teamForm.addResultToForm(WLD.WIN);
	
		
		String form = teamForm.toString();
		assertEquals("WLWLW", form);
	}
	
	//see if reseting works but adding a result and then running remove 
	@Test
	public void resetFormTest(){
		teamForm.addResultToForm(WLD.LOSS);
		teamForm.resetForm();
		
		String form = teamForm.toString();
		assertEquals("-----", form);
	}

	//test to check if number of games works
	//this should return 2 becasue i inputed two games
	@Test
	public void getNumOfGamesTest(){
		teamForm.addResultToForm(WLD.WIN);
		teamForm.addResultToForm(WLD.DRAW);
		
		int num = teamForm.getNumGames();
		assertEquals(2, num);
	}
	@Test
	public void getNumOfGamesOverflow(){
		teamForm.addResultToForm(WLD.WIN);
		teamForm.addResultToForm(WLD.DRAW);
		teamForm.addResultToForm(WLD.DRAW);
		teamForm.addResultToForm(WLD.DRAW);
		teamForm.addResultToForm(WLD.DRAW);
		teamForm.addResultToForm(WLD.DRAW);		
		teamForm.addResultToForm(WLD.WIN);
		teamForm.addResultToForm(WLD.WIN);

		
		int num = teamForm.getNumGames();
		assertEquals(5, num);
	}
	//tests if num games returns zero correctly 
	@Test
	public void getNumOfGamesZeroTest(){
		
		int num = teamForm.getNumGames();
		assertEquals(0, num);
	}
	
}