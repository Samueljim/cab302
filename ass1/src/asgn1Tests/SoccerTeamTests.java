package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn1Exceptions.LeagueException;
import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerTeam;



/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerLeage class
 *
 * @author Alan Woodley
 *
 */
public class SoccerTeamTests {
	SoccerTeam team;
	
	@Before
	public void initinal() throws TeamException{
		team = new SoccerTeam("hello", "world");
		
	}
	// Exceptions
	//tests if you can make a team with a blank offical name
	@Test(expected = TeamException.class)
	public void invallidNameTest() throws TeamException{
		team = new SoccerTeam("","test");
	}	
	//tests if you can make a team with a blank nick name
	@Test(expected = TeamException.class)
	public void invallidNameTest1() throws TeamException{
		team = new SoccerTeam("test","");
	}	
	//tests if you can make a team with a blank names
	@Test(expected = TeamException.class)
	public void invallidNameTest2() throws TeamException{
		team = new SoccerTeam("","");
	}	
	//test if you can input a too few number of goals		
	@Test(expected = TeamException.class)
	public void twoFewGoalsTest() throws TeamException{
		team.playMatch(-1, 15);
	}
	//test if you can input a too many goals		
	@Test(expected = TeamException.class)
	public void tooManyGoalsTest() throws TeamException{
		team.playMatch(10, 25);
	}
//tests play match if it changes the won, lost and draw
	@Test
	public void playMatchTest() throws TeamException{
		team.playMatch(17, 15);
		team.playMatch(10, 3);
		team.playMatch(3, 1);
		team.playMatch(2, 5);
		team.playMatch(5, 5);
		team.playMatch(10, 10);
		assertEquals(3, team.getMatchesWon());
		assertEquals(1, team.getMatchesLost());
		assertEquals(2, team.getMatchesDrawn());
	}
	//tests that get official name returns the right name
	@Test
	public void getOfficialNameTest() throws TeamException{
		assertEquals("hello", team.getOfficialName());
	}
	//tests that get nick name returns the right name
	@Test
	public void getNickNameTest() throws TeamException{
		assertEquals("world", team.getNickName());
	}
	//test if it can return zero
	@Test
	public void noMatchesWonTest() throws TeamException{
		team.playMatch(10, 15);
		team.playMatch(1, 3);
		team.playMatch(0, 1);
		assertEquals(0, team.getMatchesWon());
		assertEquals(-8, team.getGoalDifference());
		assertEquals(-8, team.getGoalDifference());
	}
	//test that toString can be called via the method
	@Test
	public void getFormStringTest() throws TeamException{
		team.playMatch(10, 15);
		team.playMatch(5, 3);
		team.playMatch(0, 1);
		assertEquals("LWL--", team.getFormString());

	}
	//test matchesdrawn 
	@Test
	public void MatchesDrawnTest() throws TeamException{
		team.playMatch(10, 10);
		team.playMatch(0,0 );
		team.playMatch(20, 20);
		assertEquals(3, team.getMatchesDrawn());
	}
	//test getCompetitionPoints
	@Test
	public void pointsTest() throws TeamException{
		team.playMatch(17, 15);
		team.playMatch(10, 3);
		team.playMatch(3, 1);
		assertEquals(9, team.getCompetitionPoints());
	}
	//test for getGoalsScoredSeason
	@Test
	public void goalsScoredSeasonTest() throws TeamException{
		team.playMatch(3, 15);
		team.playMatch(10, 3);
		team.playMatch(7, 1);
		assertEquals(20, team.getGoalsScoredSeason());
	}
	//test for getGoalsScoredSeason 2
	@Test
	public void goalsScoredSeasonTest2() throws TeamException{
		team.playMatch(17, 15);
		team.playMatch(10, 3);
		team.playMatch(3, 1);
		assertEquals(30, team.getGoalsScoredSeason());
	}
@Test
public void getGoalsConcededSeason() throws TeamException{
		team.playMatch(17, 15);
		team.playMatch(10, 3);
		team.playMatch(3, 1);
		assertEquals(19, team.getGoalsConcededSeason());
	}
//	@Test
//	public void displayTeamDetailsTest() throws TeamException{
//		assertEquals("hello world	-----	0	0	0	0	0	0	0	0", team.displayTeamDetails());
//	}
	//tests to see if reset works 
	@Test
	public void resetTest() throws TeamException{
		team.playMatch(17, 15);
		team.playMatch(10, 3);
		team.playMatch(3, 1);
		team.playMatch(3, 5);
		team.playMatch(3, 8);
		team.playMatch(15, 4);
		team.resetStats();
		assertEquals(0, team.getMatchesWon());
		assertEquals(0, team.getCompetitionPoints());
		assertEquals(0, team.getGoalDifference());
		assertEquals(0, team.getMatchesLost());
		assertEquals(0, team.getGoalsScoredSeason());

		
	}
	//test for comapreTo to see if the output is what i expect it to be for three teams
	//team1 should be -6 compared to team2 
	//team2 should be 6 compared to team3 
	//team3 should be 14 compared to team1
	@Test
	public void compareToTest() throws TeamException{
		SoccerTeam team1 = new SoccerTeam("1", "2");
		SoccerTeam team2 = new SoccerTeam("3", "4");
		SoccerTeam team3 = new SoccerTeam("5", "6");
		team1.playMatch(17, 15);
		team1.playMatch(10, 3);
		team1.playMatch(20, 1);
		team2.playMatch(1, 15);
		team2.playMatch(1, 3);
		team2.playMatch(2, 1);
		team3.playMatch(17, 15);
		team3.playMatch(13, 3);
		team3.playMatch(3, 1);
		
		int testOne = team1.compareTo(team2);
		int testTwo = team2.compareTo(team3);
		int testThree = team3.compareTo(team1);
		
		assertEquals(-6, testOne);
		assertEquals(6, testTwo);
		assertEquals(14, testThree);
		assertTrue(testOne < 0);
		assertTrue(testTwo > 0);
		assertTrue(testThree > 0);

	}
}
