package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import asgn1Exceptions.CompetitionException;
import asgn1Exceptions.LeagueException;
import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerCompetition;
import asgn1SoccerCompetition.SoccerLeague;
import asgn1SoccerCompetition.SoccerTeam;

/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerCompetition class
 *
 * @author Alan Woodley
 *
 */
public class SoccerCompetitionTests {
	SoccerLeague leg1;
	SoccerLeague leg2;
	SoccerCompetition testcom1;
	SoccerCompetition testcom2;
	SoccerCompetition testcom3;
	SoccerTeam team1;
	SoccerTeam team2;
	SoccerTeam team3;
	SoccerTeam team4;
	SoccerTeam team5;
	SoccerTeam team6;
	SoccerTeam team7;
	SoccerTeam team8;


	
	@Before
	public void setupTestLeague() throws TeamException, LeagueException{
		leg1 = new SoccerLeague(4);
		team1 = new SoccerTeam("One", "1");
		team2 = new SoccerTeam("Two", "2");
		team3 = new SoccerTeam("Three", "3");
		team4 = new SoccerTeam("Four", "4");
		
		leg2 = new SoccerLeague(4);
		team5 = new SoccerTeam("Five", "5");
		team6 = new SoccerTeam("Six", "6");
		team7 = new SoccerTeam("Seven", "7");
		team8 = new SoccerTeam("Eight", "8");
	
		
		testcom1 = new SoccerCompetition("Test 1", 1, 4);
		testcom2 = new SoccerCompetition("Test 2", 2, 4);
		testcom3 = new SoccerCompetition("Test 3", 3, 4);
		try {
			testcom1.getLeague(0).registerTeam(team1);
			testcom1.getLeague(0).registerTeam(team2);
			testcom1.getLeague(0).registerTeam(team3);
			testcom1.getLeague(0).registerTeam(team4);
		} 
		catch (CompetitionException e) {
				e.printStackTrace();
		}
		try {
			testcom2.getLeague(0).registerTeam(team1);
			testcom2.getLeague(0).registerTeam(team2);
			testcom2.getLeague(0).registerTeam(team3);
			testcom2.getLeague(0).registerTeam(team4);
			
			testcom2.getLeague(1).registerTeam(team5);
			testcom2.getLeague(1).registerTeam(team6);
			testcom2.getLeague(1).registerTeam(team7);
			testcom2.getLeague(1).registerTeam(team8);
		} 
		catch (CompetitionException e) {
			e.printStackTrace();
		}	
	}
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	SoccerCompetition comp = new SoccerCompetition("nameTest", 2, 3);
	
	//test to see if exception fails if you try to add a team to a league that isn't real
	@Test(expected = CompetitionException.class)
	public void expectionForImpossilbeLeagueTest() throws CompetitionException, LeagueException, TeamException{
		SoccerTeam team = new SoccerTeam("hello", "world");
		comp.getLeague(3).registerTeam(team);
		
		thrown.expect(LeagueException.class);
	}	
	//same as last test but for a different exception 
	@Test(expected = CompetitionException.class)
	public void expectionForImpossilbeLeague2Test() throws CompetitionException, LeagueException, TeamException{
		SoccerTeam team = new SoccerTeam("hello", "world");
		testcom1.getLeague(-1).registerTeam(team);
		
		thrown.expect(CompetitionException.class); 
	}	
	//test if season is off after it starts 
	@Test
	public void startOffSeasonTest(){
		testcom1.startSeason();
		try {
			assertFalse(testcom1.getLeague(0).isOffSeason());
		} catch (CompetitionException e) {
			e.printStackTrace();
		}
	}

		@Test
		public void getLeagueTest(){
			testcom2.startSeason();
				
					try {
						assertEquals(team2, testcom2.getLeague(0).getTeamByOfficalName("Two"));
					} catch (LeagueException | CompetitionException e) {
						e.printStackTrace();
					}
				
		}
		@Test
		public void endTeamTest(){
			testcom2.startSeason();
				
					try {
						assertEquals(team2, testcom2.getLeague(0).getTeamByOfficalName("Two"));
					} catch (LeagueException | CompetitionException e) {
						e.printStackTrace();
					}
				
		}


	
}

