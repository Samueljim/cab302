package asgn1Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import asgn1Exceptions.LeagueException;
import asgn1Exceptions.TeamException;
import asgn1SoccerCompetition.SoccerLeague;
import asgn1SoccerCompetition.SoccerTeam;


/**
 * A set of JUnit tests for the asgn1SoccerCompetition.SoccerLeage class
 *
 * @author Alan Woodley
 *
 */


public class SoccerLeagueTests {
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	SoccerLeague league = new SoccerLeague(4);

	// Exceptions
	//should throw Exception if two teams have the same name
	@Test(expected = LeagueException.class)
	public void registerTeamSameNameTest() throws LeagueException, TeamException{
		league.registerTeam(new SoccerTeam("hello", "world"));
		league.registerTeam(new SoccerTeam("hello", "world"));
		thrown.expect(LeagueException.class);

	}	
	//should throw Exception if there are more than 4 teams
	@Test(expected = LeagueException.class)
	public void registerTeamFarManyTeamsTest() throws LeagueException, TeamException{
		league.registerTeam(new SoccerTeam("1", "7"));
		league.registerTeam(new SoccerTeam("2", "8"));
		league.registerTeam(new SoccerTeam("3", "9"));
		league.registerTeam(new SoccerTeam("4", "10"));
		league.registerTeam(new SoccerTeam("5", "11"));
		league.registerTeam(new SoccerTeam("6", "12"));
	}	
	//should throw Exception you try and register a team when the season is off

	@Test(expected = LeagueException.class)
	public void registerTeamOffSeasonTest() throws LeagueException, TeamException{
		SoccerTeam team1 = new SoccerTeam("hello","world");	
		league.endSeason();
		league.registerTeam(team1);
		thrown.expect(LeagueException.class);

	}	
	//should throw excpetion if you try and remove a team when the season is off 

	@Test(expected = LeagueException.class)
	public void removeTeamOffSeasonTest() throws LeagueException, TeamException{
		SoccerTeam team1 = new SoccerTeam("hello","world");	
		league.endSeason();
		league.removeTeam(team1);
		thrown.expect(LeagueException.class);

	}	
	//should throw Exception if you try to remove a team from a leauge that isn't in the league
	@Test(expected = LeagueException.class)
	public void removeTeamRemoveNoneTest() throws LeagueException, TeamException{
		SoccerTeam team1 = new SoccerTeam("hello","world");	
		league.removeTeam(team1);
		thrown.expect(LeagueException.class);

	}	
	//test if removeTeam allows for non existing teams to be removed
	@Test(expected = LeagueException.class)
	public void removeTeamRemoveNoneTest() throws LeagueException, TeamException{
		SoccerTeam team1;
		league.removeTeam(team1);
		thrown.expect(LeagueException.class);

	}	
	//should throw Exception there are already max teams registered 
	@Test(expected = LeagueException.class)
	public void registerTeamOverflowTest() throws LeagueException, TeamException{
		SoccerTeam team1 = new SoccerTeam("1","2");	
		SoccerTeam team2 = new SoccerTeam("3","4");	
		SoccerTeam team3 = new SoccerTeam("5","6");	
		SoccerTeam team4 = new SoccerTeam("7","8");	
		SoccerTeam team5= new SoccerTeam("9","10");	
		league.registerTeam(team1);
		league.registerTeam(team2);
		league.registerTeam(team3);
		league.registerTeam(team4);
		league.registerTeam(team5);
		thrown.expect(LeagueException.class);

	}	
	//tests if the number of teams is zero if there are no Team
	@Test
	public void getRegisteredNumTeamsTest() throws LeagueException, TeamException {
		assertTrue(league.getRegisteredNumTeams() == 0);
	}
	//tests to see if adding a team makes the  number of Teams 1 
	@Test
	public void registerTeamAddTeamTest() throws LeagueException, TeamException {
		SoccerTeam team1 = new SoccerTeam("hello","world");	
		league.registerTeam(team1);
		assertTrue(league.getRegisteredNumTeams() == 1);
	}
	//add and remove test, as you can't remove a team that isn't in a leauge
	//i am going to add and then remove a team
	@Test
	public void removeTeamTest() throws LeagueException, TeamException {
		SoccerTeam team1 = new SoccerTeam("hello","world");	
		league.registerTeam(team1);
		league.removeTeam(team1);
		assertTrue(league.getRegisteredNumTeams() == 0);
	}
	//test the required (max) number of teams is set 
	@Test
	public void requiredTeamsNumTest() throws LeagueException, TeamException {
		assertTrue(league.getRequiredNumTeams() == 4);
	}
	//test for Exception if there are not enough teams registerd to start a season
	@Test (expected = LeagueException.class)
	public void startSeasonWithNotEnoughTeamsTest() throws LeagueException, TeamException {
		SoccerTeam team1 = new SoccerTeam("hello","world");	
		league.registerTeam(team1);
		league.startNewSeason();
		thrown.expect(LeagueException.class);
	}
	@Test 
	public void startsSeasonTest() throws LeagueException, TeamException {
		SoccerTeam team1 = new SoccerTeam("1","2");	
		SoccerTeam team2 = new SoccerTeam("3","4");	
		SoccerTeam team3 = new SoccerTeam("5","6");	
		SoccerTeam team4 = new SoccerTeam("7","8");	
		
		league.registerTeam(team1);
		league.registerTeam(team2);
		league.registerTeam(team3);
		league.registerTeam(team4);
	
		league.startNewSeason();
		assertEquals(false, league.isOffSeason());
	}
	//test if off season throws an exception if it's run with the season already ended
	@Test (expected = LeagueException.class)
	public void offseasonExceptionTest() throws LeagueException, TeamException {
		league.endSeason();
	}
//	tests to see if starting a new season resets the teams to defaults 
	@Test 
	public void startSeasonRestTest() throws LeagueException, TeamException {
		SoccerTeam team1 = new SoccerTeam("1","2");	
		SoccerTeam team2 = new SoccerTeam("3","4");	
		SoccerTeam team3 = new SoccerTeam("5","6");	
		SoccerTeam team4 = new SoccerTeam("7","8");	
		
		league.registerTeam(team1);
		league.registerTeam(team2);
		league.registerTeam(team3);
		league.registerTeam(team4);
		
		league.startNewSeason();
		
		assertTrue(team1.getCompetitionPoints() == 0);
		assertTrue(team1.getMatchesWon() == 0);
		assertTrue(team2.getGoalDifference() == 0);
		assertTrue(team3.getMatchesLost() == 0);
		assertTrue(team4.getGoalsScoredSeason() == 0);
		assertEquals("-----", team4.getFormString());

		//makes sure the teams are still in the list
		assertEquals("6", team3.getNickName());
		
	}
	//test that get by offical name works to return the correct name
	@Test 
	public void getTeamByOfficalNameTest() throws LeagueException, TeamException {
		SoccerTeam team1 = new SoccerTeam("1","2");	
		SoccerTeam team2 = new SoccerTeam("3","4");	
		SoccerTeam team3 = new SoccerTeam("5","6");	
		SoccerTeam team4 = new SoccerTeam("7","8");	
		
		league.registerTeam(team1);
		league.registerTeam(team2);
		league.registerTeam(team3);
		league.registerTeam(team4);
		
		assertEquals(team2, league.getTeamByOfficalName("3"));
	}
	//tests if sort and get bottom team can return the right team
	@Test
	public void testGetBottomTeam() throws LeagueException, TeamException{
		SoccerTeam team1 = new SoccerTeam("1","2");	
		SoccerTeam team2 = new SoccerTeam("3","4");	
		SoccerTeam team3 = new SoccerTeam("5","6");	
		SoccerTeam team4 = new SoccerTeam("7","8");	
		
		league.registerTeam(team1);
		league.registerTeam(team2);
		league.registerTeam(team3);
		league.registerTeam(team4);
		
	
		league.startNewSeason();
		league.playMatch("1", 5, "3", 6);
		league.sortTeams();
		assertEquals(team1, league.getBottomTeam());
	}
	//tests if sort and get top team can return the right team
	@Test
	public void testGetTopTeam() throws LeagueException, TeamException{
		SoccerTeam team1 = new SoccerTeam("1","2");	
		SoccerTeam team2 = new SoccerTeam("3","4");	
		SoccerTeam team3 = new SoccerTeam("5","6");	
		SoccerTeam team4 = new SoccerTeam("7","8");	
		
		league.registerTeam(team1);
		league.registerTeam(team2);
		league.registerTeam(team3);
		league.registerTeam(team4);
		
	
		league.startNewSeason();
		league.playMatch("1", 5, "3", 6);
		league.sortTeams();
		assertEquals(team2, league.getTopTeam());
	}
}

