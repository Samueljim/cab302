/**
 * 
 */
package asgn1SoccerCompetition;

import java.util.ArrayList;


import asgn1Exceptions.CompetitionException;
import asgn1Exceptions.LeagueException;
import asgn1SportsUtils.WLD;

/**
 * A class to model a soccer competition. The competition contains one or more number of leagues, 
 * each of which contain a number of teams. Over the course a season matches are played between
 * teams in each league. At the end of the season a premier (top ranked team) and wooden spooner 
 * (bottom ranked team) are declared for each league. If there are multiple leagues then relegation 
 * and promotions occur between the leagues.
 * 
 * @author Alan Woodley
 * @version 1.0
 *
 */
public class SoccerCompetition implements SportsCompetition{
	
	String name;
	private ArrayList<SoccerLeague> ScL;
	/**
	 * Creates the model for a new soccer competition with a specific name,
	 * number of leagues and number of teams in each league
	 * 
	 * @param name The name of the competition.
	 * @param numLeagues The number of leagues in the competition.
	 * @param numTeams The number of teams in each league.
	 */
	public SoccerCompetition(String name, int numLeagues, int numTeams){
		this.name = name;
		ScL = new ArrayList<SoccerLeague>();
        for(int iterator = 0; iterator < numLeagues; iterator++) {
        	ScL.add(new SoccerLeague(numTeams));
        }

	}
	
	/**
	 * Retrieves a league with a specific number (indexed from 0). Returns an exception if the 
	 * league number is invalid.
	 * 
	 * @param leagueNum The number of the league to return.
	 * @return A league specified by leagueNum.
	 * @throws CompetitionException if the specified league number is less than 0.
	 *  or equal to or greater than the number of leagues in the competition.
	 */
	public SoccerLeague getLeague(int leagueNum) throws CompetitionException{
		if (leagueNum < 0) 
			throw new CompetitionException("League number is less than 0");
		
		if (leagueNum > ScL.size())  
			throw new CompetitionException("League number is greater than number of leagues");
		
		Object[] sl = ScL.toArray();
		
		return (SoccerLeague)sl[leagueNum];
		
		
	}
	

	/**
	 * Starts a new soccer season for each league in the competition.
	 */
	public void startSeason() {
		for (SoccerLeague league : ScL){
			  try {
				league.startNewSeason();
			} catch (LeagueException e) {
			
				e.printStackTrace();
			}
			}
	}

	
	/** 
	 * Ends the season of each of the leagues in the competition. 
	 * If there is more than one league then it handles promotion
	 * and relegation between the leagues.  
	 * 
	 */
	public void endSeason()  {
//		SoccerLeague league1;
//		SoccerLeague league2;
//		SoccerTeam worstFromleague1;
//		SoccerTeam bestFromleague2;
		
		ArrayList<SoccerTeam> bestTeams = new ArrayList<SoccerTeam>();
		
		ArrayList<SoccerTeam> worstTeams = new ArrayList<SoccerTeam>();
		
		for (SoccerLeague league : ScL){
			
		  try {
			league.endSeason();
			
			bestTeams.add(league.getTopTeam());
			worstTeams.add(league.getBottomTeam());

		  	} catch (LeagueException e) {
		
		  		e.printStackTrace();
		  		
		  	}
		}
		//swap which league the teams are in
		if(this.ScL.size()>1){

			for(int leagueInt = 0; leagueInt < (ScL.size() - 1); leagueInt++){
				try{


					ScL.get(leagueInt).removeTeam(worstTeams.get(leagueInt));
					ScL.get(leagueInt+1).removeTeam(bestTeams.get(leagueInt+1));
					
					ScL.get(leagueInt).registerTeam(bestTeams.get(leagueInt+1));
					ScL.get(leagueInt+1).registerTeam(worstTeams.get(leagueInt));
					
				}catch(LeagueException e){
					e.printStackTrace();
					
				}
			
			}
			
		}
		

	}

	/** 
	 * For each league displays the competition standings.
	 * 
	 */
	public void displayCompetitionStandings(){
		System.out.println("+++++" + this.name + "+++++");
			int leagueInt = 1;
			for (SoccerLeague league : ScL){
				
			System.out.println("---- League" + (leagueInt) + " ----");
			
			System.out.println("Official Name" +  '\t' +  "Nick Name" + '\t' + "Form" + '\t' +  "Played" + '\t' + "Won" + '\t' + "Lost" + '\t' + "Drawn" + '\t' + "For" + '\t' + "Against" + '\t' + "GlDiff" + '\t' + "Points");
			//displays all teams standing for this league
			league.displayLeagueTable();
			//make it say the next league num
			leagueInt ++;
			}
	}

}
