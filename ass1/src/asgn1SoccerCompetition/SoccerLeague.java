package asgn1SoccerCompetition;

import java.util.ArrayList;
import java.util.Collections;


import asgn1Exceptions.LeagueException;
import asgn1Exceptions.TeamException;


/**
 * A class to model a soccer league. Matches are played between teams and points awarded for a win,
 * loss or draw. After each match teams are ranked, first by points, then by goal difference and then
 * alphabetically. 
 * 
 * @author Alan Woodley
 * @version 1.0
 *
 */
public class SoccerLeague implements SportsLeague{
	// Specifies the number of team required/limit of teams for the league
	private int requiredTeams;
	// Specifies is the league is in the off season
	private boolean offSeason;
	private  int registeredTeams;
	
	private  ArrayList<SoccerTeam> socTeam;
	
	/**
	 * Generates a model of a soccer league with the specified number of teams. 
	 * A season can not start until that specific number of teams has been added. 
	 * Once that number of teams has been reached no more teams can be added unless
	 * a team is first removed. 
	 * 
	 * @param requiredTeams The number of teams required/limit for the league.
	 */
	public SoccerLeague (int requiredTeams){
		
		this.requiredTeams = requiredTeams;
		this.registeredTeams = 0;
		this.offSeason = true;
      
		socTeam = new ArrayList<SoccerTeam>(requiredTeams);
	}

	
	/**
	 * Registers a team to the league.
	 * 
	 * @param team Registers a team to play in the league.
	 * @throws LeagueException If the season has already started, if the maximum number of 
	 * teams allowed to register has already been reached or a team with the 
	 * same official name has already been registered.
	 */
	public void registerTeam(SoccerTeam team) throws LeagueException {		
		if (!(isOffSeason())) 
		  throw new LeagueException("Season Already started");
		if (registeredTeams >= requiredTeams) 
			  throw new LeagueException("Too many teams");
		if (this.containsTeam(team.getOfficialName())) 
			  throw new LeagueException("Name already used");
		
		socTeam.add(team);
		this.registeredTeams+=1;
	}
	
	/**
	 * Removes a team from the league.
	 * 
	 * @param team The team to remove
	 * @throws LeagueException if the season has not ended or if the team is not registered into the league.
	 */
	public void removeTeam(SoccerTeam team) throws LeagueException{
		if (!isOffSeason())
			 throw new LeagueException("It's off season");
		
		if(!(this.containsTeam(team.getOfficialName())))
			throw new LeagueException("Team is not registered into this leauge");
		
//		this.socTeam.removeIf(teamInList-> teamInList.getOfficialName().equals(team.getOfficialName()));
		socTeam.remove(team);
		this.registeredTeams-=1;
		
	}
	
	/** 
	 * Gets the number of teams currently registered to the league
	 * 
	 * @return the current number of teams registered
	 */
	public int getRegisteredNumTeams(){
		return this.registeredTeams;
	}
	
	/**```
	 * Gets the number of teams required for the league to begin its 
	 * season which is also the maximum number of teams that can be registered
	 * to a league.

	 * @return The number of teams required by the league/maximum number of teams in the league
	 */
	public int getRequiredNumTeams(){
		return this.requiredTeams;
	}
	
	/** 
	 * Starts a new season by reverting all statistics for each team to initial values.
	 * 
	 * @throws LeagueException if the number of registered teams does not equal the required number of teams or if the season has already started
	 */
	public void startNewSeason() throws LeagueException{
		if (!isOffSeason())
			  throw new LeagueException("The season has already started");
		if (getRequiredNumTeams() != getRegisteredNumTeams())
		  throw new LeagueException("invailed teams numbers");
      for (SoccerTeam team : socTeam){
        	team.resetStats();
      }
      this.offSeason = false;
	}
	

	/**
	 * Ends the season.
	 * 
	 * @throws LeagueException if season has not started
	 */
	public void endSeason() throws LeagueException{
		
		if(offSeason)
	      throw new LeagueException("Season has not started");
		
		offSeason = true;
	}
	
	/**
	 * Specifies if the league is in the off season (i.e. when matches are not played).
	 * @return True If the league is in its off season, false otherwise.
	 */
	public boolean isOffSeason(){
		return this.offSeason;
	}
	
	
	
	/**
	 * Returns a team with a specific name by cheaking the teams \
	 * 
	 * 
	 * @param name The official name of the team to search for.
	 * @return The team object with the specified official name.
	 * @throws LeagueException if no team has that official name.
	 * 
	 * 
	 */
	public SoccerTeam getTeamByOfficalName(String name) throws LeagueException{		
        boolean found_name = false;
		
//		for(int nameInt = 0; nameInt < getRegisteredNumTeams(); nameInt++){
		  for(SoccerTeam team : socTeam){
			  if(name.equals(team.getOfficialName())){
				  found_name = true;
				  return team;
			  }
		  }
		
		if(!found_name)
		  throw new LeagueException("No teams by that official name");
		return null;
		
//		return socTeam.get(9999);
	}
		
	/**
	 * Plays a match in a specified league between two teams with the respective goals. After each match the teams are
	 * resorted.
     *
	 * @param homeTeamName The name of the home team.
	 * @param homeTeamGoals The number of goals scored by the home team.
	 * @param awayTeamName The name of the away team.
	 * @param awayTeamGoals The number of goals scored by the away team.
	 * @throws LeagueException If the season has not started or if both teams have the same official name. 
	 */
	public void playMatch(String homeTeamName, int homeTeamGoals, String awayTeamName, int awayTeamGoals) throws LeagueException{
		
		if(isOffSeason())
	      throw new LeagueException("Season hasn't started");
		if(homeTeamName.equals(awayTeamName)){
			throw new LeagueException("Same team");
		}
		SoccerTeam team1 = getTeamByOfficalName(homeTeamName);
		SoccerTeam team2 = getTeamByOfficalName(awayTeamName);
		try{
			team1.playMatch(homeTeamGoals, awayTeamGoals);
		}catch (TeamException e) {
			e.printStackTrace();
			
		}
		try{
			team2.playMatch(awayTeamGoals, homeTeamGoals);
		}catch (TeamException e) {
			e.printStackTrace();
		}
		sortTeams();
	}
	
	/**
	 * Displays a ranked list of the teams in the league to the screen.
	 */
	public void displayLeagueTable(){
		
		for (SoccerTeam team : socTeam){
			team.displayTeamDetails();
		}
	}	
	
	/**
	 * Returns the highest ranked team in the league.
     *
	 * @return The highest ranked team in the league. 
	 * @throws LeagueException if the number of teams is zero or less than the required number of teams.
	 */
	public SoccerTeam getTopTeam() throws LeagueException{
	
		if(socTeam.size() <= 0) 
		  throw new LeagueException("There are no teams in this league");
		if (requiredTeams > socTeam.size())
			  throw new LeagueException("Too many teams for the League");
		
	
	
		return socTeam.get(0);
		

	}

	/**
	 * Returns the lowest ranked team in the league.
     *
	 * @return The lowest ranked team in the league. 
	 * @throws LeagueException if the number of teams is zero or less than the required number of teams.
	 */
	public SoccerTeam getBottomTeam() throws LeagueException{
		
		if (socTeam.size() <= 0)
		  throw new LeagueException("There are less than required number of teams");
		if (requiredTeams > socTeam.size())
			  throw new LeagueException("Too many teams for the League");
	
		
		return socTeam.get(socTeam.size() - 1);
		
	}

	/** 
	 * Sorts the teams in the league.
	 */
    public void sortTeams(){		
    	Collections.sort(socTeam);
    }
    
    /**
     * Specifies if a team with the given official name is registered to the league.
     * 
     * @param name The name of a team.
     * @return True if the team is registered to the league, false otherwise. 
     */
    public boolean containsTeam(String name){
		boolean state = false; 
		for (SoccerTeam team : socTeam){
			if (team.getOfficialName().equals(name))
				state = true;
		}
    	return state;
    }
    
}
